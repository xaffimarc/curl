<?php
/**
 * My library of useful Drupal functions
 *
              o
|\   \\\\__     o
| \_/    o \   o o
> _   ((  <_ o(CarkFish)
| / \__+___/  o o
|/     |/       o

 *
 */

function _cark_create_product($machine_name, $human_name ){
/*  $machine_name = 'gift_voucher';
  $human_name = 'Gift Voucher';*/

  _cark_variation_save($machine_name, $human_name);

  // Create the product display type.
  $type = node_type_set_defaults();
  $type->type = $machine_name;
  $type->name = $human_name;
  $type->custom = TRUE;
  $type->base = 'node_content';
  $status = node_type_save($type);

  // node_type_save doesn't invoke this.
  field_attach_create_bundle('node', $machine_name);

  // Don't display author information
  variable_set('node_submitted_' . $machine_name, 0);

  // Remove promoted to front page.
  variable_set('node_options_' . $machine_name, array('status'));


  $entity_info = entity_get_info('node');
  // Update the weight of the "title" field. - This is default code from the "core" equivalent and seems to be necessary when adding custom fields
  if (!empty($entity_info['field replacement'])) {
    if ($instance = field_info_instance('node', 'title_field', $machine_name)) {
      if (isset($instance['display'])) {
        foreach ($instance['display'] as $view_mode_key => $view_mode_settings) {
          $instance['display'][$view_mode_key]['weight'] = -20;
          $instance['display'][$view_mode_key]['type'] = 'title_linked';
          $instance['display'][$view_mode_key]['label'] = 'hidden';
          $instance['display'][$view_mode_key]['module'] = 'title';
          $instance['display'][$view_mode_key]['settings']['title_link'] = 'content';
        }
      }
      field_update_instance($instance);
    }
  }

  //Updates the database cache of node types
  node_types_rebuild();

  //Returns a list of all the available node types.
  $types = node_type_get_types();

  //Adds default body field to a node type.
  node_add_body_field($types[$machine_name]);


}

/*
 * Save product variations
 */
function _cark_variation_save($machine_name, $human_name){
  // Create the basic product type.
  $product_type = commerce_product_ui_product_type_new();

  $product_type['type'] = $machine_name;
  $product_type['name'] = $human_name;
  $product_type['description'] = $human_name;
  $product_type['is_new'] = TRUE;

  commerce_product_ui_product_type_save($product_type, TRUE);

  drupal_set_message(t('Product type saved.'));

}



/**
 * @param $fieldname
 * @param $fieldtype
 * @return array
 *
 * This function creates the field entities and then calls a function to assign them as instances
 *
 * Types:
 * commerce_product_reference
 * text
 */
//_cark_create_field(['The machine name of the field'], ['The drupal field type you wish to create'], ['The target entities machine name']){
// Note you can also call this function on existing fields and attach them to different targets
function _cark_create_field($field_machine_name, $field_type, $target_entity){


  switch ($field_type){
    case 'commerce_product_reference':
      $cark_field =  array(
        $field_machine_name => array(
          'field_name' => $field_machine_name,
          'type' => $field_type,
          'cardinality' => FIELD_CARDINALITY_UNLIMITED,
          'entity_types' => array('node'),
          'translatable' => FALSE,
          'locked' => FALSE,
        ),
      );
      break;
  }



//Create the field if it doesn't already exist
  foreach ($cark_field as $field) {
    $thisfield = field_info_field($field);
    if (!$thisfield) {
      field_create_field($field);
    }
  }
  //run the function to assign an instance
  _cark_field_instance($target_entity, 'commerce_product_reference');

}


/**
 * Create field instances on entities
 */
function _cark_field_instance($entity_machine_name, $field_type){
  switch ($field_type) {
    case 'commerce_product_reference':
      $cark_instance = array(
        $entity_machine_name => array(
          'field_name' => 'field_product',
          'entity_type' => 'node',
          'bundle' => $entity_machine_name,
          'label' => 'Product variations',
          'required' => TRUE,
          'settings' => array(
            'field_injection' => 1,
            'referenceable_types' => array(
              $entity_machine_name => $entity_machine_name,
            ),
            'user_register_form' => FALSE,
          ),
          'widget' => array(
            'active' => 1,
            'module' => 'inline_entity_form',
            'settings' => array(
              'fields' => array(),
              'type_settings' => array(
                'allow_existing' => 0,
                'autogenerate_title' => 1,
                'delete_references' => 1,
                'match_operator' => 'CONTAINS',
                'use_variation_language' => 1,
              ),
            ),
            'type' => 'inline_entity_form',
          ),
          'display' => array(
            'default' => array(
              'label' => 'above',
              'module' => 'commerce_cart',
              'settings' => array(
                'attributes_single' => 1,
                'combine' => 0,
                'default_quantity' => '1',
                'line_item_type' => 'product',
                'show_quantity' => 0,
                'show_single_product_attributes' => FALSE,
              ),
              'type' => 'commerce_cart_add_to_cart_form',
              'weight' => '3',
            ),
            'full' => array(
              'label' => 'hidden',
              'module' => 'commerce_cart',
              'settings' => array(
                'combine' => 01,
                'default_quantity' => '1',
                'line_item_type' => 'product',
                'show_quantity' => 0,
                'show_single_product_attributes' => FALSE,
              ),
              'type' => 'commerce_cart_add_to_cart_form',
              'weight' => '5',
            ),
          ),
        ),
      );
      break;
  }
  foreach ($cark_instance as $instance) {
    field_create_instance($instance);
  }
}


/**
 * Custom function to delete all instances of a given entity in a given bundle
 */
function _cark_delete_entities($entity_type, $bundle_type){
  // We must load all commerce products here
  $results = entity_load($entity_type);

  foreach($results as $key => $value){
    if($value->type == $bundle_type){
      // Delete the entity
      entity_delete($entity_type, $key);
    }
  }
  // Now we can delete the entity type itself
  if (entity_load($entity_type)) {
    commerce_product_ui_product_type_delete($entity_type);
  }
}

/**
 * Deletes all instances of a particular node type and then the node type itself
 */
function _cark_delete_node($node_type){
  $nids = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => $node_type))
    ->fetchCol();

  if (!empty($nids)){
    node_delete_multiple($nids);
  }
  if (node_type_load($node_type)){
    node_type_delete($node_type);
    variable_del('node_preview_' . $node_type);
  }
  node_types_rebuild();
  menu_rebuild();
}