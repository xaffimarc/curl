<?php

/**
 * Field Management functions
 */
function _sd_fields($type_name){
  $break = "";
  $t = get_t();
  return array(
    'field_product' => array(
      'field_name' => 'field_product',
      'type' => 'commerce_product_reference',
      'cardinality' => FIELD_CARDINALITY_UNLIMITED,
      'entity_types' => array('node'),
      'translatable' => FALSE,
      'locked' => FALSE,
    ),
/*    'field_my_test_field2' => array(
      'field_name' => 'field_my_test_field2',
      'type' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
    ),*/
/*    'field_voucher_expiry_date' => array(
      'field_name' => 'field_voucher_expiry_date',
      'type' => 'datetime',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'settings' => array(
        'granularity' => array(
          'month' => 'month',
          'day' => 'day',
          'year' => 'year',
          'hour' => 0,
          'minute' => 0,
          'second' => 0,
        ),
        'tz_handling' => 'none',
        'timezone_db' => '',
        'cache_enabled' => 0,
        'cache_count' => '4',
        'todate' => '',
      ),
    ),*/
  );
}
function _sd_fields_instances($type_name){
  $t = get_t();
  return array(
    'field_product' => array(
      'field_name' => 'field_product',
      'entity_type' => 'node',
      'bundle' => $type_name,
      'label' => 'Product variations',
      'required' => TRUE,
      'settings' => array(
        'field_injection' => 1,
        'referenceable_types' => array(
          $type_name => $type_name,
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'inline_entity_form',
        'settings' => array(
          'fields' => array(),
          'type_settings' => array(
            'allow_existing' => 0,
            'autogenerate_title' => 1,
            'delete_references' => 1,
            'match_operator' => 'CONTAINS',
            'use_variation_language' => 1,
          ),
        ),
        'type' => 'inline_entity_form',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'commerce_cart',
          'settings' => array(
            'attributes_single' => 1,
            'combine' => 0,
            'default_quantity' => '1',
            'line_item_type' => 'product',
            'show_quantity' => 0,
            'show_single_product_attributes' => FALSE,
          ),
          'type' => 'commerce_cart_add_to_cart_form',
          'weight' => '3',
        ),
        'full' => array(
          'label' => 'hidden',
          'module' => 'commerce_cart',
          'settings' => array(
            'combine' => 01,
            'default_quantity' => '1',
            'line_item_type' => 'product',
            'show_quantity' => 0,
            'show_single_product_attributes' => FALSE,
          ),
          'type' => 'commerce_cart_add_to_cart_form',
          'weight' => '5',
        ),
      ),
    ),
 /*   'field_my_test_field2' => array(
      'field_name' => 'field_my_test_field2',
      'entity_type' => 'node',
      'bundle' => $type_name,
      'label' => 'my test feild 2',
      'widget' => array(
        'weight' => '-3',
        'type' => 'text_textfield',
        'active' => 1,
        'settings' => array(
          'size' => '60',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
          'settings' => array(),
          'weight' => -3,
        ),
      ),
      'required' => 0,
      'description' => '',
      'fences_wrapper' => 'div_div_div',
      'default_value' => NULL,
      'field_name' => 'field_my_test_feild2',
      'entity_type' => 'commerce_order',
      'bundle' => 'commerce_order',
    ),*/
  );
}


function _sd_order_fields($type_name){
  $field = array(
    'field_used_voucher_code' => array(
    'translatable' => '0',
    'settings' => array(
      'max_length' => '255',
    ),
    'field_name' => 'field_used_voucher_code',
    'type' => 'text',
    'active' => '1',
    'locked' => '0',
    'cardinality' => '1',
    ),
  );
  return $field;
}
function _sd_order_fields_instance($type_name){
  $instance = array(
    'field_used_voucher_code' => array(
    'label' => 'Voucher Code Used',
    'widget' => array(
      'weight' => '-3',
      'type' => 'text_textfield',
      'active' => 1,
      'settings' => array(
        'size' => '60',
      ),
    ),
    'settings' => array(
      'text_processing' => '0',
      'user_register_form' => FALSE,
      'referenceable_types' => array(
        $type_name => $type_name,
      ),
    ),
    'display' => array(
      'default' => array(
        'label' => 'above',
        'type' => 'text_default',
        'settings' => array(),
        'weight' => -3,
      ),
    ),
    'required' => 0,
    'description' => '',
    'fences_wrapper' => 'div_div_div',
    'default_value' => NULL,
    'field_name' => 'field_used_voucher_code',
    'entity_type' => 'commerce_order',
    'bundle' => 'commerce_order',
    ),
  );
  return $instance;
}