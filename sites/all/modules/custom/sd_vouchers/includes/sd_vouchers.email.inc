<?php
/**
 * TODO - The rule that uses this action needs to be created at install for the module to be truly self contained
 */


/**
 * This is actually a rules action, and so should really be in the rules.inc file, however,
 * due to this rule containing important data pertaining only to the email I feel it makes more sense to have it here
 */
function _sd_vouchers_send_mail($order){
  $line_items = $order->commerce_line_items['und'];

  //todo make the recipient the order owner when ready for proper testing
 // $email = 'xaffimarc@gmail.com';
  $email = $order->mail;
  $line_items = $order->commerce_line_items['und'];

  $email_body = '<div class="header"><div class="logo"><a href="https://www.gardenlines.co.uk"><img src="https://www.gardenlines.co.uk/sites/default/files/gardenlines-logo_0.png"></a></div></div>';
  $email_body .= "<div style='height:10px; width:100%; border-bottom:3px solid green;'><br/><br/></div><br/><h3>Your Gift Voucher</h3><p>";
  $email_body .= "<div style='margin-top:20px;'>";

  // First we must check if the order contains a gift voucher
  foreach($line_items as $key => $value){
    $line_item = commerce_line_item_load($value['line_item_id']);
    if($line_item->type == "product"){
      $product = commerce_product_load($line_item->commerce_product['und']['0']['product_id']);
      if($product->type == "gift_voucher"){

        $email_body .= "<div>";
        $email_body .= "<h2>Voucher Code : " . $line_item->field_voucher_code['und'][0]['value'] . "</h2>";
        $email_body .= "<h2>Value : £" . $line_item->commerce_total['und'][0]['amount']/100 . "</h2>";
        $email_body .= "</div>";
      }
    }
  }
  $email_body .= "</div>";

  $params = array(
    'subject' => 'Your Gift Voucher',
    'body' => $email_body,
  );

  // Send out the e-mail.
  drupal_mail('sd_vouchers', 'sd_vouchers_mail_example', $email, language_default(), $params);
  drupal_set_message("Your gift voucher has been dispatched to " . $email . ". Remember to check your spam folder if your voucher doesn't arrive");
}



/**
 * Implementation of hook_mail().
 */
function sd_vouchers_mail($key, &$message, $params){

  // Set the mail content type to html to send an html e-mail (optional).
  $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';

  // Grab the subject and body from params and add it to the message.
  $message['subject'] = $params['subject'];
  $message['body'][] = $params['body']; // Drupal 7
  //$message['body'] = $params['body']; // Drupal 6, I think

  switch ($key) {
    case "sd_vouchers_mail_example":
      // Make other changes here if desired...
      break;
  }
}
