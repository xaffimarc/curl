<?php

class sd_voucher {
  var $gv_id = NULL;
  var $order_id = NULL;
  var $purchaser = NULL;
  var $owner = NULL;
  var $voucher_code = NULL;
  var $key = NULL;
  var $balance = NULL;
  var $initial_value = NULL;
  var $vouchertax = NULL;
  var $valid = FALSE;

  /**
   * Builds a basic voucher object depending on the parameters supplied
   */
  function __construct($full_voucher_code = NULL){
    if($full_voucher_code == NULL){
      /**
       * We are making a brand new voucher, so generate some stuffs
       */
      $this->voucher_code = $this->_voucher_generate();
      $this->key = $this->_voucher_key_gen();
    }else{
      /**
       * We are "potentially" dealing with an existing voucher (or a fake one)
      */
      $this->_voucher_validate($full_voucher_code);
    }
  }


  /**
   * Add a new gift voucher to the database as it's purchased
   */
  function _voucher_save($order, $product, $line_item){
    $r = FALSE;

    $voucher_code = $this->voucher_code;
    $voucher_key = $this->key;
    /**
     * TODO, Encrypt the voucher code and key before adding to database - MD5 / SHA1?
     */


    // We check the voucher doesn't already exist
    if($this->_voucher_validate($voucher_code . $voucher_key) == false){
      /**
       * We need to add the voucher code to the line item so we can actually use it later
       */
      $line_item->field_voucher_code['und'][0]['value'] = $voucher_code . $voucher_key;
      commerce_line_item_save($line_item);

      $price = $product->commerce_price['und'][0]['amount'] / 100;
      $tax_type = $product->commerce_price['und'][0]['data']['include_tax'];
      $tax_rate = commerce_tax_rate_load($tax_type);

      //insert the new voucher code in the database table
        db_insert('sd_vouchers')->fields(array(
        'purchaser' => $order->uid,
        'order_id' => $order->order_number,
        'vouchercode' => $voucher_code,
        'voucherkey' => $voucher_key,
        'balance' => $price,
        'initial_balance' => $price,
        'voucher_tax' => $tax_rate['rate'],
      ))->execute();

      $insert_successful = FALSE; // todo check if insert was successful.
      if ($insert_successful) {
        $r = TRUE;
      }
    }


    return $r;
  }

  /**
   * TEST VOUCHER CODE : 44395397254063758332
   */
  function _voucher_validate($voucher_code){
    $real_vouchercode = substr($voucher_code, 0 ,16);
    $supplied_key = substr($voucher_code, -4);

    //Use Database API to retrieve the voucher if it exists.
    $query = db_select('sd_vouchers');
    $query->fields('sd_vouchers', array('gv_id', 'order_id', 'purchaser', 'owner', 'vouchercode', 'voucherkey', 'balance', 'initial_balance', 'voucher_tax'));
    $query->condition('vouchercode', $real_vouchercode); //Published.
    $query->condition('voucherkey', $supplied_key); //Published.
    $result =  $query->execute()->fetchAssoc();

    if($result !== FALSE && $result['balance'] > 0){
      $this->valid = TRUE;
      $this->gv_id = $result['gv_id'];
      $this->order_id = $result['order_id'];
      $this->purchaser = $result['purchaser'];
      $this->owner = $result['owner'];
      $this->voucher_code = $result['vouchercode'];
      $this->key = $result['voucherkey'];
      $this->balance = $result['balance'];
      $this->initial_value = $result['initial_balance'];
      $this->vouchertax = $result['vouchertax'];

      if($this->owner != NULL && $this->owner != $GLOBALS['user']->uid){
        $break = "";
        // if the voucher owner has been set the voucher owner should match the current user
        $this->valid = FALSE;
      }


    } else{
      $this->valid = FALSE;
    }
    return $result;
  }

  function _voucher_apply($order = NULL){
    if($order != NULL){
      //Add the voucher code to the order
      $order->field_used_voucher_code['und'][0]['value'] = $this->voucher_code . $this->key;
      $break = "";

      // Call the rule that applies the discount
      _sd_vouchers_discount_fixed_amount($order, $this);
    }
  }

  /**
   * @param null $order
   * This function is called when an order with an applied gift voucher has been completed successfully
   */
  function _voucher_update($order = NULL){
  /**
   * This function should be called from checkout complete in the main module code
   **/

    $line_items = $order->commerce_line_items['und'];
    // First we must check if the order contains a gift voucher
    foreach($line_items as $key => $value) {
      $line_item = commerce_line_item_load($value['line_item_id']);
     if ($line_item->type == "gift_voucher") {

       $previous_balance = $this->balance*100;
       $remianing_balance = $previous_balance + $line_item->commerce_total['und'][0]['amount'];

       //Use Database API to retrieve the voucher.
       //Update the voucher in the database table


       db_update('sd_vouchers')
         ->fields(array('balance' => $remianing_balance/100, 'owner' => $GLOBALS['user']->uid))
         ->condition ('vouchercode', $this->voucher_code, '=')
         ->execute();

       //The voucher has been updated so now we need to add a line to the transaction log table

       $order_date = date("Y-m-d H:i:s", $order->changed);

       //insert the new voucher code in the database table
        db_insert('sd_vouchers_transactions')->fields(array(
         'vouchercode' => $this->voucher_code . $this->key,
         'user_id' => $order->order_number,
         'order_id' => $order->uid,
         'redeemed_value' => $line_item->commerce_total['und'][0]['amount']/100,
         'order_date' => $order_date,
       ))->execute();

       $insert_successful = FALSE; // todo check if insert was successful.
       if ($insert_successful) {
         $r = TRUE;
       }
      }
    }
  }


  /**
   * Custom function to generate the voucher code
   */
  function _voucher_generate(){
    $voucher_code = array();

    for ($x = 1; $x <= 15; $x++) {
      $voucher_code[$x] = rand(0,9);
    }
    if($x == 16){
      $voucher_code[]= $this->_luhn($voucher_code);
    }
    return implode("", $voucher_code);
  }

  /**
   * Solve the Luhn algorithm for this voucher code
   * For a detailed explanation of how this works see the functional spec
   * ***IMPORTANT***
   * In terms of the luhn alghorithm, odd is even and even is odd. This is because the array does not yet contain its 16th member
   */
  function _luhn($voucher_code){
    // Reverse the array
    $voucher_code_reverse = array();
    $voucher_code_reverse = array_reverse($voucher_code, true);

    $luhn_even = array();
    $luhn_odd = array();

    foreach($voucher_code_reverse as $key => $value){
      if($key % 2 != 0){
        //it is odd for us and even for the number as a whole
        $luhn_even[$key] = $value * 2;
        if($luhn_even[$key] >= 10){
          $luhn_even[$key] = array_sum(str_split($luhn_even[$key]));
        }
      }else{
        $luhn_odd[] = $value;
      }
    }
    $digital_root = array_sum($luhn_even);
    $odd_sum = array_sum($luhn_odd);

    $digital_total = $digital_root + $odd_sum;

    if($digital_total % 10){
      $next_base =  $digital_total + (10 - $digital_total % 10);
      $check_digit = $next_base - $digital_total;
    }else{
      $check_digit = $digital_total % 10;
    }
    return $check_digit;
  }

  /**
   * Custom function to generate the voucher secret key
   */
  function _voucher_key_gen(){
    $voucher_key = str_pad(rand(0,9999), 4, "0", STR_PAD_LEFT);
    return $voucher_key;
  }


}


