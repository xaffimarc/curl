<?php
/**
 * This file configures an extra checkout pane for showing the voucher code entry form on any step during checkout
 * You will need to go to : /admin/commerce/config/checkout
 * And assign the "Voucher form" pane to the section in which you want it to appear
 *
 * Further this file handles the callback of said form and instantiates the voucher object for further use
 *
 */



/**
* Configure the voucher checkout pane.
*/
/*function sd_tester_checkout_pane_settings_form($checkout_pane) {
  $form['sd_tester_checkout_pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('TESTER title'),
    '#default_value' => variable_get('sd_tester_checkout_pane_title', ''),
  );
  $form['sd_tester_checkout_pane_description'] = array(
    '#type' => 'textarea',
    '#title' => t('tester pane description'),
    '#default_value' => variable_get('sd_tester_checkout_pane_description', ''),
  );
  return $form;
}*/

/**
 * Define the form to show to customers within the pane
 */
/*function sd_tester_checkout_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  global $user;
  $form = array();
  $form['tester']['code'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a Gift Voucher'),
      '#default_value' => '',
      '#required' => FALSE,
    );
/*  $form['button'] = array(
    '#type' => 'button',
    '#value' => t('TESTY TEST TEST'),
    '#ajax' => array(
      'callback' => '_handle_form_submit',
    ),
  );*/
  return $form;
}*/





