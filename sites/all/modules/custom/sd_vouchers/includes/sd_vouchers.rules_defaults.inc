<?php
function sd_vouchers_default_rules_configuration() {
  $configs = array();
  $rule = '{ "rules_voucher_bought" : {
    "LABEL" : "Voucher bought",
    "PLUGIN" : "reaction rule",
    "OWNER" : "rules",
    "TAGS" : [ "vouchers" ],
    "REQUIRES" : [ "commerce_order", "sd_vouchers", "commerce_checkout" ],
    "ON" : { "commerce_checkout_complete" : [] },
    "IF" : [
      { "commerce_order_contains_product_type" : {
          "commerce_order" : [ "commerce_order" ],
          "product_type" : { "value" : { "gift_voucher" : "gift_voucher" } },
          "operator" : "\u003E=",
          "value" : "1"
        }
      }
    ],
    "DO" : [ { "_sd_vouchers_send_mail" : { "order" : [ "commerce-order" ] } } ]
  }
}';
  $configs['rules_voucher_bought'] = rules_import($rule);
  return $configs;
}

/**
 * { "rules_voucher_test" : {
"LABEL" : "Voucher Test",
"PLUGIN" : "reaction rule",
"OWNER" : "rules",
"REQUIRES" : [ "sd_vouchers" ],
"ON" : { "gift_voucher_added" : [] },
"DO" : [
{ "_sd_vouchers_discount_fixed_amount" : { "order" : [ "site:current-cart-order" ] } }
]
}
}
 */
