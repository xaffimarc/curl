<?php
/**
 * This file configures an extra checkout pane for showing the voucher code entry form on any step during checkout
 * You will need to go to : /admin/commerce/config/checkout
 * And assign the "Voucher form" pane to the section in which you want it to appear
 *
 * Further this file handles the callback of said form and instantiates the voucher object for further use
 *
 */



/**
* Configure the voucher checkout pane.
*/
function sd_vouchers_checkout_pane_settings_form($checkout_pane) {
  $form['sd_vouchers_checkout_pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Checkout pane title'),
    '#default_value' => variable_get('sd_vouchers_checkout_pane_title', ''),
  );
  $form['sd_vouchers_checkout_pane_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Checkout pane description'),
    '#default_value' => variable_get('sd_vouchers_checkout_pane_description', ''),
  );
  return $form;
}

/**
 * Define the form to show to customers within the pane
 */
function sd_vouchers_checkout_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  global $user;
  $form = array();
  $form['voucher']['code'] = array(
      '#type' => 'textfield',
      '#title' => t('Enter a Gift Voucher'),
      '#default_value' => '',
      '#required' => FALSE,
    );
  $form['button'] = array(
    '#type' => 'button',
    '#value' => t('Pay With Gift Voucher'),
    '#ajax' => array(
      'callback' => '_handle_form_submit',
    ),
  );
  return $form;
}






/**
 * Custom function to allow us to reload the current page after the ajax call
 * Relies on Ctools module
 */
function _pagereloader($order_id){
  ctools_include('ajax');
  ctools_add_js('ajax-responder');
// Path to redirect to

  //todo check the order id is valid and present
  $path = 'checkout/' . $order_id . '/review';
  $commands[] = ctools_ajax_command_redirect($path);

  print ajax_render($commands);
  drupal_exit();
}

