<?php
/**
 * TODO - The rule that uses this action needs to be created at install for the module to be truly self contained
 */

/**
 * Define Rules event to make things go BOOM!!!
 * Implementation of hook_rules_event_info().
 * @ingroup rules
 */
function sd_vouchers_rules_event_info() {
  return array(
    'gift_voucher_added' => array(
      'label' => t('A gift Voucher has been applied successfully'),
      'module' => 'sd_vouchers',
      //'variables' => entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order'))),
      'variables' => array(
        'order' => entity_rules_events_variables('commerce_order', t('Order', array(), array('context' => 'a drupal commerce order'))),
      ),
    ),
  );
}

/**
 * Implement hook_rules_action_info()
 * Declare any meta data about actions for Rules
 */
function sd_vouchers_rules_action_info() {
  $actions = array(
    '_sd_vouchers_send_mail' => array(
      'label' => t('Send HTML email reminding customer of their order'),
      'group' => t('Rules Example'),
      'parameter' => array(
        'order' => array(
          'type' => 'commerce_order',
          'label' => 'Order of the current user',
        ),
      ),
    ),
    '_sd_vouchers_discount_fixed_amount' => array(
      'label' => t('Apply a discount rate'),
      'group' => t('Rules Example'),
      'parameter' => array(
        'order' => array(
          'type' => 'commerce_order',
          'label' => 'Order of the current user',
        ),
      ),
    ),

  );
  return $actions;
}

/**
 * Rules action: Apply fixed amount discount.
 */
function _sd_vouchers_discount_fixed_amount($order, $voucher) {

  //todo use Language_none not und
  $line_items = $order->commerce_line_items[LANGUAGE_NONE];
  // First we must check if the order contains a gift voucher
  $voucher_present = FALSE;

  foreach($line_items as $key => $value){
    $line_item = commerce_line_item_load($value['line_item_id']);
    if($line_item->type == 'gift_voucher'){
      $voucher_present = TRUE;
      if(commerce_line_item_delete($line_item->line_item_id)){
        $voucher_present = FALSE;
      }
    }
  }

  // We only add a voucher if there isnt one already in place
  if($voucher_present == FALSE){
    $line_item = commerce_line_item_new('gift_voucher', $order->order_id);

    $line_item->quantity = 1;
    $line_item->line_item_label = 'gift_voucher';

    // Wrap the line item and product to easily set field information.
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    $line_item_wrapper->commerce_total->amount = 0;	// Just establish field
    //$line_item_wrapper->info['property defaults']['voucher'] = $voucher->voucher_code;	// Just establish field


    /**
     * Lets decide how much of the voucher value to use
     */
    $voucher_balance = -$voucher->balance * 100;
    $order_total = $order->commerce_order_total['und'][0]['amount'];

    if($voucher_balance >= -$order_total){
      // The value of the voucher is less than the value of the order, so we use the entire balance
      $price = -$voucher->balance * 100;
    }else if($voucher_balance <= -$order_total){
      // The voucher has a higher value than the order, so we essentially make the order free and will leave the balance on the voucher
      $price = -$order_total;
    }
    $line_item_wrapper->commerce_unit_price->amount = $price;
    $curr_code = $order->commerce_order_total[LANGUAGE_NONE][0]['currency_code'];

    /**
     * Really important to notice the price component element here, this is what makes the voucher show in the order summary breakdown
     * See here for more details :: http://www.drupalcontrib.org/api/drupal/contributions!commerce!modules!price!commerce_price.api.php/function/hook_commerce_price_component_type_info/7
     */
    $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
      $line_item_wrapper->commerce_unit_price->value(),
        'base_price',
        array(
          'amount' => 0,
          'currency_code' => $curr_code,
          'data' => array(),
        ),
        FALSE
    );
    // todo make this a custom price component and then we can check against it
    $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
      $line_item_wrapper->commerce_unit_price->value(),
      'discount',
      array(
        'amount' => $price,
        'currency_code' => $curr_code,
        'data' => array(),
      ),
      FALSE
    );
    // Save the line item now so we get its ID.
    //$line_item = commerce_line_item_save($line_item);
    commerce_line_item_save($line_item);

    // Add it to the order's line item reference value.
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $order_wrapper->commerce_line_items[] = $line_item;

    //commerce_discount_remove_discount_components($order_wrapper->commerce_order_total);

    commerce_order_save($order);
  }
  /**
   * Reload the page using ctools (module has this as a dependency)
   */
  // In your ajax form submit callback
  ctools_include('ajax');
  ctools_add_js('ajax-responder');
  $commands[] = ctools_ajax_command_reload();
  print ajax_render($commands);
  drupal_exit();
}


