<?php

//$urlfilename = "1-25";
//$urlfilename = "26-50";
//$urlfilename = "51-75";
//$urlfilename = "76-101";
//$urlfilename = "101-125";
//$urlfilename = "126-150";
//$urlfilename = "151-175";
//$urlfilename = "176-200";
//$urlfilename = "201-225";
//$urlfilename = "226-250";
//$urlfilename = "251-275";
//$urlfilename = "276-300";
$urlfilename = "301-325";


// Defining the basic cURL function
function curl($url) {
  // Assigning cURL options to an array
  $options = Array(
    CURLOPT_RETURNTRANSFER => TRUE,  // Setting cURL's option to return the webpage data
    CURLOPT_FOLLOWLOCATION => TRUE,  // Setting cURL to follow 'location' HTTP headers
    CURLOPT_AUTOREFERER => TRUE, // Automatically set the referer where following 'location' HTTP headers
    CURLOPT_CONNECTTIMEOUT => 120,   // Setting the amount of time (in seconds) before the request times out
    CURLOPT_TIMEOUT => 120,  // Setting the maximum amount of time for cURL to execute queries
    CURLOPT_MAXREDIRS => 10, // Setting the maximum number of redirections to follow
    CURLOPT_USERAGENT => "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1a2pre) Gecko/2008073000 Shredder/3.0a2pre ThunderBrowse/3.2.1.8",  // Setting the useragent
    CURLOPT_URL => $url, // Setting cURL's URL option with the $url variable passed into the function
    CURLOPT_COOKIE => "AuthToken=01c4feea-5361-4d60-8fdb-72cd1fdd27c9",
  );

  $ch = curl_init();  // Initialising cURL
  curl_setopt_array($ch, $options);   // Setting cURL's options using the previously assigned array data in $options
  $data = curl_exec($ch); // Executing the cURL request and assigning the returned data to the $data variable
  curl_close($ch);    // Closing cURL
  return $data;   // Returning the data from the function
}


// Defining the basic scraping function
function scrape_between($data, $start, $end){
  $data = stristr($data, $start); // Stripping all data from before $start
  $data = substr($data, strlen($start));  // Stripping $start
  $stop = stripos($data, $end);   // Getting the position of the $end of the data to scrape
  $data = substr($data, 0, $stop);    // Stripping all data from after and including the $end of the data to scrape
  return $data;   // Returning the scraped data from the function
}








function _get_urls($urlfilename){
    $row = 1;
    $url_list = array();
    if (($handle = fopen("urls/urls_" . $urlfilename . ".csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            $num = count($data);
            $url_list[] = $data['0'];
            $row++;
            /*        for ($c=0; $c < $num; $c++) {
                        echo $data[$c] . "<br />\n";
                    }*/
        }
        fclose($handle);
    }
    return $url_list;
}


function _get_fields(){
    $row = 1;
    $field_list = array();
    if (($handle = fopen("fields.csv", "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, "|")) !== FALSE) {
            $num = count($data);
            $field_list[] = $data['0'];
            $row++;
    /*        for ($c=0; $c < $num; $c++) {
                echo $data[$c] . "<br />\n";
            }*/
        }
        fclose($handle);
    }
    return $field_list;
}

// Executing our curl function to scrape the webpage http://www.example.com and return the results into the $scraped_website variable
//$scraped_page = curl("http://www.runnersworld.co.uk/events/viewevent.asp?sp=&v=1&EN=79328&ms=");

//Get the html from every URL in the URLs array in turn
$url_list = _get_urls($urlfilename);
$htmlarray = array();
foreach($url_list as $row){
    $htmlarray[] = curl($row);
    set_time_limit(30);
}

$break = "";



/*
 * Now we start to parse the HTMLs we have retrieved using Simple Html DOM
 */
include('simple_html_dom.php');
$field_list = _get_fields();

$xml = '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
$xml .= '<rwdata>' . "\n";

foreach($htmlarray as $htmlpage){
    $html = str_get_html($htmlpage);
    $main_table = $html->find('div[id=local]', 0);
    $xml .= '<event>' . "\n";
    $x = 1;

    $event_title = $main_table->find('h1[class=mainEvntTitle]', 0)->plaintext;
    $xml .= "<title>" . htmlspecialchars($event_title) . "</title>"  . "\n";
  $break= "";
    foreach($main_table->find('td') as $row){
        $html_value = str_replace(":", "", $row->plaintext);
/*        if(strpos($html_value, 'Monday') == true || strpos($html_value, 'Tuesday') == true || strpos($html_value, 'Wednesday') == true|| strpos($html_value, 'Thursday') == true || strpos($html_value, 'Friday') == true || strpos($html_value, 'Saturday') == true || strpos($html_value, 'Sunday') == true){
            $xml .= "<eventdate>" . htmlspecialchars($html_value) . "</eventdate>";
            $break ="";
        }*/

        if(in_array($html_value, $field_list)){

            $field_name = trim(preg_replace('/[^A-Za-z0-9\-]/', '', $html_value)); // Removes special chars.
            $x++;
        }else if(!in_array($html_value, $field_list) && $x >= 2 && $html_value != ""){
            $break ="";
            $html_value = preg_replace("#\R+#", " ", $html_value); //remove end of lines in fields
            $xml .= "<" . $field_name . ">" . htmlspecialchars($html_value) . "</" . $field_name . ">" . "\n";




            $field_name = "";
            $x = 1;
            $break ="";
        }

    }
/*
    //**********OLD LOOP
    foreach($main_table->find('td') as $row){
        //$test = explode(": ", $row->plaintext);
        $html_value = str_replace(":", "", $row->plaintext);
        $break= "";
        if(in_array($html_value, $field_list)){

            $field_name = trim(preg_replace('/[^A-Za-z0-9\-]/', '', $html_value)); // Removes special chars.
            $break= "";

        }else if(isset($field_name) && $field_name != NULL){

            //$field_value = trim($html_value); // Removes special chars.
            $field_value = strip_tags($html_value); // Removes special chars.
            if($field_name == "Address"){
                $field_value = preg_replace('/[^\da-z]/i', '', $field_value);
            }

            //$field_value = preg_replace('/[^\da-z]/i', '', $field_value);
            if($field_value != "" && isset($field_value)){
                $xml .= "<" . $field_name . ">" . htmlspecialchars($field_value) . "</" . $field_name . ">";
                $break= "";
            }

            $field_name = NULL;

        }else{
            $field_name = NULL;
        }

        $x++;

        $break= "";


    }
    //**********OLD LOOP
*/
    $xml .= '</event>' . "\n";
}




$xml .= '</rwdata>';
echo $xml;

$file = 'outputs/output_' . $urlfilename . '.xml';


// Write the contents back to the file
file_put_contents($file, $xml);




//$event_title = $main_table->find('h1[class=mainEvntTitle]', 0)->plaintext;
?>